package com.iosu.sheddtestapp.data

import com.iosu.sheddtestapp.domain.search.TopicImage
import com.iosu.sheddtestapp.domain.search.TopicRepository

class DummyTopicRepository : TopicRepository {

    override fun findImages(text: String): List<TopicImage> =
            listOf(TopicImage("https://pbs.twimg.com/profile_images/749902175342563332/oS2kM4MZ_400x400.jpg"))

}