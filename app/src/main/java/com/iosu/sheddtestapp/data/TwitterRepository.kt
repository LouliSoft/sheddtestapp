package com.iosu.sheddtestapp.data

import com.iosu.sheddtestapp.domain.search.TopicImage
import com.iosu.sheddtestapp.domain.search.TopicRepository
import com.twitter.sdk.android.core.services.SearchService

class TwitterRepository(private val searchService: SearchService) : TopicRepository {

    override fun findImages(text: String): List<TopicImage> =
            searchService.tweets(text,
                    null,
                    "en",
                    "en_GB",
                    null,
                    30,
                    null,
                    null,
                    null,
                    true).execute().body().tweets
                    .filter { it.entities.media.isNotEmpty() } // We want images tweets
                    .flatMap { it.entities.media } // then we flatten the array of images
                    .map { TopicImage(it.mediaUrl) } // add url to the resulting list

}