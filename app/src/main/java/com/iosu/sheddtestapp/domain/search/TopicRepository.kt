package com.iosu.sheddtestapp.domain.search

interface TopicRepository {

    fun findImages(text: String): List<TopicImage>

}