package com.iosu.sheddtestapp.presentation.search

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import com.iosu.sheddtestapp.R


class ImagesItemView : FrameLayout {

    private var imageView: AppCompatImageView? = null

    constructor(context: Context) : super(context) {
        initializeView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeView()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initializeView()
    }

    fun decorate(imageUrl: String) {
        imageView?.let { Glide.with(this).load(imageUrl).into(it) }
    }

    private fun initializeView() {

        val itemView = View.inflate(context, R.layout.view_images_item, this)

        imageView = itemView.findViewById(R.id.view_images_item_image)
    }
}