package com.iosu.sheddtestapp.presentation.main

import android.app.Application
import com.iosu.sheddtestapp.data.DummyTopicRepository
import com.iosu.sheddtestapp.data.TwitterRepository
import com.iosu.sheddtestapp.domain.search.TopicRepository
import com.iosu.sheddtestapp.presentation.search.SearchViewModel
import com.twitter.sdk.android.core.*
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

class SheddApplication : Application() {

    // Koin modules
    private val searchViewModel : Module = applicationContext {
        viewModel { SearchViewModel(get()) } // get() will resolve Repository instance
        bean { DummyTopicRepository() as TopicRepository }
        bean { TwitterRepository(TwitterCore.getInstance().apiClient.searchService) as TopicRepository }
    }

    override fun onCreate() {
        super.onCreate()

        // Start Koin
        startKoin(this, listOf(searchViewModel))

        Twitter.initialize(TwitterConfig.Builder(this)
                .twitterAuthConfig(TwitterAuthConfig("tJgMey7Ar5g1oHkO4FcRzbTvd", "wnsdGfhSxDVKr7NOewW9G8nfwwsvSgFvePcSBePeFNN9Ngq3Xg"))
                .debug(BuildConfig.DEBUG).build())
    }
}