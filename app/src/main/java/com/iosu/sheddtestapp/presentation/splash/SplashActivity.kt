package com.iosu.sheddtestapp.presentation.splash

import android.os.Bundle
import android.os.Handler
import com.iosu.sheddtestapp.R
import com.iosu.sheddtestapp.presentation.main.BaseActivity
import com.iosu.sheddtestapp.presentation.search.SearchActivity

class SplashActivity : BaseActivity() {

    private val splashTimer: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({ startActivity(SearchActivity.makeIntent(this)) }, splashTimer)
    }
}