package com.iosu.sheddtestapp.presentation.search

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import com.iosu.sheddtestapp.R
import com.iosu.sheddtestapp.domain.search.TopicImage
import com.iosu.sheddtestapp.presentation.main.BaseActivity
import com.iosu.sheddtestapp.presentation.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.android.architecture.ext.viewModel


class SearchActivity : BaseActivity(), SearchView.OnQueryTextListener {

    // Inject SearchViewModel
    private val searchViewModel: SearchViewModel by viewModel()
    private var adapter: SearchItemsAdapter? = null

    private var lastTextTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu.findItem(R.id.search)
        val searchView = searchMenuItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.isSubmitButtonEnabled = true
        searchView.setOnQueryTextListener(this)

        return true
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null && System.currentTimeMillis() - lastTextTime > 350 && isNetworkAvailable(this)) {
            updateImages(searchViewModel.getImages(newText))
        }
        lastTextTime = System.currentTimeMillis()

        return true
    }

    private fun updateImages(list: List<TopicImage>) {
        if (adapter != null) {
            adapter?.updateList(list)
        } else {
            adapter = SearchItemsAdapter(list)
            recycler_view.adapter = adapter
        }
    }

    companion object {
        fun makeIntent(context: Context): Intent {
            return Intent(context, SearchActivity::class.java)
        }
    }
}