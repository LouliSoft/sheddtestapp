package com.iosu.sheddtestapp.presentation.search

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.iosu.sheddtestapp.domain.search.TopicImage

class SearchItemsAdapter(imagesList: List<TopicImage>) : RecyclerView.Adapter<SearchItemsAdapter.ItemViewHolder>() {

    private val mutableList = imagesList.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ItemViewHolder(ImagesItemView(parent.context))

    override fun getItemCount(): Int = mutableList.count()

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.decorate(mutableList[position].image)
    }

    fun updateList(imagesList: List<TopicImage>) {
        mutableList.clear()
        mutableList.addAll(imagesList)
        notifyDataSetChanged()
    }

    inner class ItemViewHolder(private val view: ImagesItemView) : RecyclerView.ViewHolder(view) {

        fun decorate(imageUrl: String) {

            view.decorate(imageUrl)
        }
    }

}