package com.iosu.sheddtestapp.presentation.search

import android.arch.lifecycle.ViewModel
import com.iosu.sheddtestapp.domain.search.TopicImage
import com.iosu.sheddtestapp.domain.search.TopicRepository

class SearchViewModel(private val topicRepository: TopicRepository) : ViewModel() {

    fun getImages(text: String) : List<TopicImage> = topicRepository.findImages(text)

}